# WaveFunction

A small project to test the Wave Function collapsing, 
see [WaveFunctionCollapse](https://github.com/mxgmn/WaveFunctionCollapse).

Made using [Processing.org](https://processing.org/).
