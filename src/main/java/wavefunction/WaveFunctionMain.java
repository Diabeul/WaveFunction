package wavefunction;

import processing.core.PApplet;
import processing.core.PImage;
import wavefunction.obj.Grid;
import wavefunction.obj.Tile;

import java.util.*;
import java.util.stream.IntStream;

public class WaveFunctionMain extends PApplet {
  //TODO: TextureManager
  Map<Integer, PImage> textures;
  List<Tile> tiles = new ArrayList<>();

  final static int DIM = 20;
  Grid grid = new Grid(DIM);

  public static void main(String[] args) {
    PApplet.main(WaveFunctionMain.class);
  }

  public void settings() {
    size(800, 800);
    smooth(0);
  }

  public void setup() {
    noStroke();
    // Circuits
//    textures = loadTextures("circuits", 14);
//    tiles.add(new Tile(textures.get(0), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(1), Arrays.asList("AAA", "ABA", "ABA", "ABA")));
//    tiles.add(new Tile(textures.get(2), Arrays.asList("ABA", "AAA", "ABA", "AAA")));
//    tiles.add(new Tile(textures.get(3), Arrays.asList("ABA", "ABA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(4), Arrays.asList("ABA", "ACA", "ABA", "ACA")));
//    tiles.add(new Tile(textures.get(5), Arrays.asList("DDD", "DDD", "DDD", "DDD")));
//    tiles.add(new Tile(textures.get(6), Arrays.asList("ABA", "AAD", "DDD", "DAA")));
//    tiles.add(new Tile(textures.get(7), Arrays.asList("AAA", "AAA", "AAD", "DAA")));
//    tiles.add(new Tile(textures.get(8), Arrays.asList("ABA", "ABA", "ABA", "ABA")));
//    tiles.add(new Tile(textures.get(9), Arrays.asList("ABA", "ABA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(10), Arrays.asList("ACA", "AAA", "ABA", "AAA")));
//    tiles.add(new Tile(textures.get(11), Arrays.asList("AAA", "ABA", "AAA", "ABA")));
//    tiles.add(new Tile(textures.get(12), Arrays.asList("ABA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(13), Arrays.asList("AAA", "ACA", "AAA", "ACA")));

    // Knot
    textures = loadTextures("knot", 15);
    tiles.add(new Tile(textures.get(0), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
    tiles.add(new Tile(textures.get(1), Arrays.asList("AAA", "ABA", "AAA", "ABA")));
    tiles.add(new Tile(textures.get(2), Arrays.asList("ABA", "ABA", "AAA", "AAA")));
    tiles.add(new Tile(textures.get(3), Arrays.asList("AAA", "ABA", "ABA", "ABA")));
    tiles.add(new Tile(textures.get(4), Arrays.asList("ABA", "ABA", "ABA", "ABA")));
    tiles.add(new Tile(textures.get(5), Arrays.asList("ABA", "ABA", "ABA", "ABA")));
//    tiles.add(new Tile(textures.get(6), Arrays.asList("ABA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(7), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(8), Arrays.asList("BBB", "ABB", "ABA", "BBA")));
//    tiles.add(new Tile(textures.get(9), Arrays.asList("BAA", "AAA", "ABA", "BBA")));
//    tiles.add(new Tile(textures.get(10), Arrays.asList("BBB", "ABB", "AAA", "BBA")));
//    tiles.add(new Tile(textures.get(11), Arrays.asList("BBB", "ABB", "ABA", "BBA")));
//    tiles.add(new Tile(textures.get(12), Arrays.asList("AAB", "ABB", "ABA", "AAA")));
//    tiles.add(new Tile(textures.get(13), Arrays.asList("AAB", "ABB", "AAB", "ABB")));
//    tiles.add(new Tile(textures.get(14), Arrays.asList("BAA", "BBA", "BAA", "BBA")));

    // Room
//    textures = loadTextures("room", 9);
//    tiles.add(new Tile(textures.get(0), Arrays.asList("BBB", "BBB", "BBB", "BBB")));
//    tiles.add(new Tile(textures.get(1), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(2), Arrays.asList("ABB", "BBA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(3), Arrays.asList("BBA", "ABB", "BBB", "BBB")));
//    tiles.add(new Tile(textures.get(4), Arrays.asList("BAB", "BBB", "BAB", "BBB")));
//    tiles.add(new Tile(textures.get(5), Arrays.asList("AAA", "ABB", "BAB", "BBA")));
//    tiles.add(new Tile(textures.get(6), Arrays.asList("BBB", "BBA", "AAA", "ABB")));
//    tiles.add(new Tile(textures.get(7), Arrays.asList("BBB", "BAB", "BAB", "BAB")));
//    tiles.add(new Tile(textures.get(8), Arrays.asList("BAB", "BAB", "BBB", "BBB")));

    // floor
//    textures = loadTextures("floor", 17);
//    tiles.add(new Tile(textures.get(0), Arrays.asList("BBB", "BBB", "BBB", "BBB")));
//    tiles.add(new Tile(textures.get(1), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(2), Arrays.asList("AAA", "ACA", "AAA", "ACA")));
//    tiles.add(new Tile(textures.get(3), Arrays.asList("AAA", "ACA", "ACA", "ACA")));
//    tiles.add(new Tile(textures.get(4), Arrays.asList("ACA", "ACA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(5), Arrays.asList("AAA", "ACA", "AAA", "ACA")));
//    tiles.add(new Tile(textures.get(6), Arrays.asList("AAA", "ADB", "BBB", "BDA")));
//    tiles.add(new Tile(textures.get(7), Arrays.asList("AAA", "ADB", "BBB", "BEA")));
//    tiles.add(new Tile(textures.get(8), Arrays.asList("AAA", "AAA", "AEB", "BEA")));
//    tiles.add(new Tile(textures.get(9), Arrays.asList("BEA", "AEB", "BBB", "BBB")));
//    tiles.add(new Tile(textures.get(10), Arrays.asList("AAA", "ACA", "AAA", "ACA")));
//    tiles.add(new Tile(textures.get(11), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(12), Arrays.asList("AAA", "AAA", "AAA", "AAA")));
//    tiles.add(new Tile(textures.get(13), Arrays.asList("AAA", "AEB", "BBB", "BEA")));
//    tiles.add(new Tile(textures.get(14), Arrays.asList("AAA", "AEB", "BBB", "BEA")));
//    tiles.add(new Tile(textures.get(15), Arrays.asList("ACA", "AEB", "BBB", "BEA")));
//    tiles.add(new Tile(textures.get(16), Arrays.asList("AAA", "ACA", "AAA", "ACA")));

    tiles = tiles.stream().map(t -> t.getRotationsWithoutDuplicate(this)).flatMap(List::stream).toList();

    // Generate adjacency rules based on edges
    tiles.forEach(tile -> tile.analyze(tiles));

    this.grid.init(tiles.size());
  }

  public void mousePressed() {
    System.out.println("Manuel collapsing");
    grid.randomCollapse(tiles);
  }

  public void draw() {
    background(0);
//    noLoop();
    IntStream.range(0, 100).forEach(i -> grid.randomCollapse(tiles));

    for (int i = 0; i < DIM; i++) {
      for (int j = 0; j < DIM; j++) {
        if (!grid.get(i, j).isCollapsed()) {
          continue;
        }

        int w = width / DIM;
        int h = height / DIM;
        image(tiles.get(grid.getOption(i, j)).getImage(), w * j, h * i, w, h);
      }
    }
  }

  public PImage rotate(PImage image, int i) {
    var newImg = createGraphics(image.width, image.height);
    newImg.beginDraw();
    newImg.imageMode(CENTER);
    newImg.translate(image.width / 2, image.height / 2);
    newImg.rotate(HALF_PI * i);
    newImg.image(image, 0, 0);
    newImg.endDraw();
    return newImg;
  }

  private Map<Integer, PImage> loadTextures(String name, int size) {
    String basePath = "src/main/resources/assets/"+name+"/";
    String extension = ".png";

    Map<Integer, PImage> textures = new HashMap<>();
    for (int i = 0; i < size; i++) {
      textures.put(i, loadImage(basePath + str(i) + extension));
    }
    return textures;
  }
}
