package wavefunction.obj;

import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class Cell {
  private boolean collapsed = false;
  private List<Integer> options;

  public Cell(List<Integer> options) {
    this.options = options;
  }

  public Cell(int options) {
    this(IntStream.range(0, options).boxed().collect(Collectors.toList()));
  }

  public void collapse(int option) {
    this.collapsed = true;
    this.options = Arrays.asList(option);
  }
}
