package wavefunction.obj;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Grid extends ArrayList<Cell> {

  private final int initialCap;

  public Grid(int initialCap) {
    super(initialCap);
    this.initialCap = initialCap;
  }

  public void init(int size) {
    for (int i = 0; i < initialCap * initialCap; i++) {
      this.add(new Cell(size));
    }
  }

  public Cell get(int i, int j) {
    return get(j + i * initialCap);
  }

  public int getOption(int i, int j) {
    return get(i, j).getOptions().get(0);
  }

  private void wave(int index, int option, List<Tile> tiles) {
    wave(index, option, tiles, -1);
  }

  private void wave(int index, int option, List<Tile> tiles, int dir) {
    if (index < 0) return;
    if (index >= initialCap * initialCap) return;

    Cell cell = get(index);
    if (dir != -1) {
      if (cell.isCollapsed()) return;

      List<Integer> rules = tiles.get(option).getRules().get(dir);
      cell.getOptions().retainAll(rules);
      if (cell.getOptions().size() == 1) {
        this.collapse(index, cell.getOptions().get(0), tiles);
      }
      return;
    }

    //LEFT
    wave(index - 1, option, tiles, 3);

    //UP
    wave(index - initialCap, option, tiles, 0);

    //RIGHT
    wave(index + 1, option, tiles, 1);

    //DOWN
    wave(index + initialCap, option, tiles, 2);
  }

  public void collapse(int index, int option, List<Tile> tiles) {
    get(index).collapse(option);
    wave(index, option, tiles);
  }

  public void randomCollapse(List<Tile> tiles) {
    Cell min = this.stream().filter(a -> a.getOptions().size() > 1).min(Comparator.comparingInt(a -> a.getOptions().size())).orElse(this.get(0));
    List<Integer> options = min.getOptions();
    this.collapse(indexOf(min), options.get(new Random().nextInt(options.size())), tiles);
  }
}
