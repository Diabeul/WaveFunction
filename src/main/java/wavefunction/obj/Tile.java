package wavefunction.obj;

import lombok.Getter;
import lombok.Setter;
import processing.core.PImage;
import wavefunction.WaveFunctionMain;
import wavefunction.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

@Getter
@Setter
public class Tile {

  private PImage image;
  private List<String> neighbours;

  private List<List<Integer>> rules = new ArrayList<>(4);

  public Tile(PImage image, List<String> neighbours) {
    this.image = image;
    this.neighbours = neighbours;
    IntStream.range(0, 4).forEach(i -> rules.add(new ArrayList<>()));
  }

  public Tile rotate(int i, WaveFunctionMain main) {
    ArrayList<String> newNeighbours = new ArrayList<>(neighbours);
    Collections.rotate(newNeighbours, i);
    return new Tile(main.rotate(this.image, i), newNeighbours);
  }

  public List<Tile> getRotationsWithoutDuplicate(WaveFunctionMain main) {
    return IntStream.range(0, 4).mapToObj(i -> this.rotate(i, main)).distinct().toList();
  }

  public void analyze(List<Tile> tiles) {
    IntStream.range(0, tiles.size()).forEach(i -> this.analyze(tiles.get(i), i));
  }

  public void analyze(Tile tile, int i) {
    IntStream.range(0, 4).filter(j -> compareNeighbour(tile, j))
        .forEach(j -> this.rules.get(j).add(i));
  }

  private boolean compareNeighbour(Tile tile, int i) {
    return StringUtils.reverse(tile.neighbours.get((i + 2) % 4)).equals(this.neighbours.get(i));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Tile tile)) return false;
    return Objects.equals(neighbours, tile.neighbours);
  }

  @Override
  public int hashCode() {
    return Objects.hash(neighbours);
  }

}
