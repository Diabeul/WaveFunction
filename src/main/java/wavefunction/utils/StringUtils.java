package wavefunction.utils;

public class StringUtils {

  private StringUtils() {}

  public static String reverse(String s) {
    return new StringBuilder(s).reverse().toString();
  }
}
